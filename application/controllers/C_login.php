<?php
	class C_login extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('M_admin');
		}

		function login(){
			$nik = $this->input->POST('nik');
			$password = $this->input->POST('password');
			$where = array(
				'NIK' => $nik,
				'password' => $password
				);
			$cek = $this->M_admin->cek_login("admin",$where)->num_rows();
			$a = $this->M_admin->cek_login("admin",$where)->result_array();
			if($cek > 0){
				foreach ($a as $d) {}
				$data_session = array(
					'NIK' => $nik,
					'nama' => $d['nama'],
					'password' => $password,
					'posisi' => $d['posisi'],
					'status' => "logged",
					'app' => "nmsjasatirta"
					);

				$this->session->set_userdata($data_session);
				redirect(base_url());
			}else{
				echo '<script type="text/javascript">alert("username atau password salah!!");</script>';
				echo "<script>history.go(-1);</script>";
				//$this->load->view('v_login');
			}
		}

		function logout(){
			$this->session->sess_destroy();
			redirect(base_url());
		}

		function cek_ketersediaan_user(){
	     	$where = array(
				'nik' => $_POST["username"],
				);

        if($this->M_admin->is_user_available($where)) {
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> NIK Sudah Terdaftar</label>';
        }
        else {
            echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> NIK Tersedia</label>';
        }
	 	}

		function aksi_regis(){
			  $nama = $this->input->POST('nama');
			  $nik = $this->input->POST('nik');
			  $posisi = $this->input->POST('posisi');
			  $password = $this->input->POST('password');
			  $confirmpass = $this->input->POST('confirmpass');

			  $data = array(
			   'nama' => $nama,
			   'NIK' => $nik,
			   'posisi' => $posisi,
			   'password' => $password,
			   );

			  $where = array(
				'NIK' => $nik,
				);

	            if($this->M_admin->is_user_available($where)) {
	                echo '<script type="text/javascript">alert("NIK sudah terdaftar !!");</script>';
			  		$this->load->view('v_register');
	            }
	            else {
	                if ($password != $confirmpass) {
					  		echo '<script type="text/javascript">alert("password tidak cocok !!");</script>';
					  		$this->load->view('v_register');
					  }
					  else{
						$this->M_admin->tambah_admin($data,'admin');
						redirect(base_url('C_admin/daftar_admin'));
					  }
	            }

		}

	}
