<?php
	class C_nms extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('M_nms');
			$this->load->helper('url');
	        $this->load->library('pagination');
	        $this->load->database();
		}

		function index(){
			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					redirect(base_url('C_nms/nms'));
				}
			}else{
				$this->load->view('v_login');
			}
		}

		function nmsdata(){
        $data = $this->M_nms->list_pelanggan();
				echo json_encode($data);
		}

		function nms(){
			$config['base_url']=base_url("C_nms/nms");
      $config['total_rows']= $this->db->query("SELECT * FROM customer")->num_rows();
      $config['per_page']=18;
   		$config['num_links'] = 2;
      $config['uri_segment']=3;
      $config['first_link']='< first ';
      $config['last_link']='last > ';
      $config['next_link']='> ';
      $config['prev_link']='< ';

      $config['full_tag_open'] = "<ul class='pagination'>";
      $config['full_tag_close'] ="</ul>";
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
      $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
      $config['next_tag_open'] = "<li>";
      $config['next_tagl_close'] = "</li>";
      $config['prev_tag_open'] = "<li>";
      $config['prev_tagl_close'] = "</li>";
      $config['first_tag_open'] = "<li>";
      $config['first_tagl_close'] = "</li>";
      $config['last_tag_open'] = "<li>";
      $config['last_tagl_close'] = "</li>";
      $this->pagination->initialize($config);

			$src['customer'] = $this->M_nms->list_nms($config);
			$src['total'] = $this->M_nms->total_nms();
			$src['totaldown'] = $this->M_nms->total_nmsdown();

			// info for header
			$header['title'] = "JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "home";

			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					// load page
					$this->load->view('header', $header);
					$this->load->view('v_home', $src);
				}
			}else{
				$this->load->view('v_login');
			}

		}

		function download(){
			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					$data = array(
						$this->M_nms->createcsv());
				}
			}else{
				$this->load->view('v_login');
			}

        }

	}
