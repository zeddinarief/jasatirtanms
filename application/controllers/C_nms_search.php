<?php
	class C_nms_search extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('M_nms');
		}

		function search(){
    	$key = $this->input->GET('search');
    	$this->src['keyword'] = $this->input->GET('search');
			$this->src['hasil'] = $this->M_nms->cari($key);

			$header['title'] = "Search | JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "other";

    	if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					// load page
					$this->load->view('header', $header);
					$this->load->view('v_home_search', $this->src);
				}
			}else{
				$this->load->view('v_login');
			}
    }

    function realtime_search($key){
    	$this->src['keyword'] = $key;
			$this->src['hasil'] = $this->M_nms->cari($key);

			$header['title'] = "Search | JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "other";

    	if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					// load page
					$this->load->view('header', $header);
					$this->load->view('v_home_search', $this->src);
				}
			}else{
				$this->load->view('v_login');
			}

    }

	}
