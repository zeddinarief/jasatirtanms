<?php
	class C_admin extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('M_admin');
		}

		function profile(){
			// info for header
			$header['title'] = "Profil | JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "other";

			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					$this->load->view('header', $header);
					$this->load->view('v_profile');
				}
			}else{
				$this->load->view('v_login');
			}

		}

		function update_profile(){
			  $nama = $this->input->POST('nama');

			  $data = array(
			   'nama' => $nama,

			   );

			   $nik = $this->session->userdata('NIK');
			   $password = $this->session->userdata('password');
			   $posisi = $this->session->userdata('posisi');

			   $where = array(
				'NIK' => $nik
				);
			  	$this->load->model("M_admin");
				$this->M_admin->update_profile($where,$data,'admin');

				$data_session = array(
					'NIK' => $nik,
					'nama' => $nama,
					'password' => $password,
					'posisi' => $posisi,
					'status' => "logged",
					'app' => "nmsjasatirta"
					);

				$this->session->set_userdata($data_session);
		 		if($this->session->userdata('app') != "nmsjasatirta"){
					$this->load->view('v_login');
				}else{
					echo '<script type="text/javascript">alert("Perubahan profil anda disimpan !!");</script>';
					echo "<script>history.go(-1);</script>";
				}

		}

		function update_pass(){
			  $oldpass = $this->input->POST('oldpassword');
			  $newpass = $this->input->POST('newpassword');
			  $confirmpass = $this->input->POST('confirmpassword');
			  $pass = $this->session->userdata('password');

			  if ($oldpass != $pass) {
			  	echo '<script type="text/javascript">alert("password lama anda salah !!");</script>';
			  		$this->load->view('v_profile');
			  	# code...
			  }
			  else{

			  		if ($newpass != $confirmpass) {
					  		echo '<script type="text/javascript">alert("password tidak cocok !!");</script>';
					  		$this->load->view('v_profile');
					}
					else{
						$data = array(
						   'password' => $newpass
						   );

						   $nik = $this->session->userdata('NIK');
						   $nama = $this->session->userdata('nama');
						   $posisi = $this->session->userdata('posisi');

						   $where = array(
							'NIK' => $nik
							);
							$this->M_admin->update_profile($where,$data,'admin');

							$data_session = array(
								'NIK' => $nik,
								'nama' => $nama,
								'password' => $newpass,
								'posisi' => $posisi,
								'status' => "logged",
								'app' => "nmsjasatirta"
								);

							$this->session->set_userdata($data_session);
							if($this->session->userdata('app') != "nmsjasatirta"){
								$this->load->view('v_login');
							}else{
								echo '<script type="text/javascript">alert("password anda telah terganti !!");</script>';
								//$this->load->view('v_profile_a');
								echo "<script>history.go(-1);</script>";
							}

					}

			  }
		}

		function daftar_admin(){
			// info for header
			$header['title'] = "Admin | JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "other";

			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					$this->src['admin'] = $this->M_admin->list_admin('admin');
					if($this->session->userdata('posisi') == "admin"){
						// load page
						$this->load->view('header', $header);
						$this->load->view('v_list_admin', $this->src);
					}else{
						redirect(base_url());
					}
					//$this->load->view('v_home_down', $this->src);
				}
			}else{
				$this->load->view('v_login');
			}
		}

		function lihatPass(){
			$niktujuan = $this->input->POST('niktujuan');
			$nik = $this->input->POST('nik');
			$where = array(
				'NIK' => $nik
				);

			$cek = $this->M_admin->lihat_pass('admin', $where)->num_rows();
			$a = $this->M_admin->lihat_pass('admin', $where)->result_array();
			if ($nik == $niktujuan) {
				if($this->session->userdata('app') != "nmsjasatirta"){
					$this->load->view('v_login');
				}else{
					if($this->session->userdata('posisi') == "admin"){
						if($cek > 0){
							foreach ($a as $d) {}
							$pass = $d['password'];
							echo '<script type="text/javascript">alert("Password dari NIK '.$nik.' = `'.$pass.'`");</script>';
							echo "<script>history.go(-1);</script>";
						}else{
							echo '<script type="text/javascript">alert("NIK salah!!");</script>';
							echo "<script>history.go(-1);</script>";
						}
					}else{
						redirect(base_url());
					}
					
				}
			}else{
				echo '<script type="text/javascript">alert("NIK salah!!");</script>';
				echo "<script>history.go(-1);</script>";
			}

		}

		function hapus_admin($nik){
			$where = array(
				'NIK' => $nik
				);

			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					if($this->session->userdata('posisi') == "admin"){
						$this->M_admin->hapus('admin',$where);
						echo "<script>history.go(-1);</script>";
					}else{
						redirect(base_url());
					}
				}
			}else{
				$this->load->view('v_login');
			}
		}

	}
