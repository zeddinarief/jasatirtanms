<?php
	class C_nms_down extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('M_nms');
			$this->load->helper('url');
	        $this->load->library('pagination');
	        $this->load->database();
		}

		function down(){
			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					redirect(base_url('C_nms_down/nms'));
				}
			}else{
				$this->load->view('v_login');
			}

		}

		function nms(){
			$config['base_url']=base_url("C_nms_down/nms");
      $config['total_rows']= $this->db->query("SELECT * FROM customer WHERE status='not_ok'")->num_rows();
      $config['per_page']=18;
   		$config['num_links'] = 2;
      $config['uri_segment']=3;
      $config['first_link']='< first ';
      $config['last_link']='last > ';
      $config['next_link']='> ';
			$config['full_tag_close'] ="</ul>";
      $config['prev_link']='< ';

      $config['full_tag_open'] = "<ul class='pagination'>";
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
      $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
      $config['next_tag_open'] = "<li>";
      $config['next_tagl_close'] = "</li>";
      $config['prev_tag_open'] = "<li>";
      $config['prev_tagl_close'] = "</li>";
      $config['first_tag_open'] = "<li>";
      $config['first_tagl_close'] = "</li>";
      $config['last_tag_open'] = "<li>";
      $config['last_tagl_close'] = "</li>";
      $this->pagination->initialize($config);

			$src['customer'] = $this->M_nms->list_nms_down($config);
			$src['total'] = $this->M_nms->total_nms();
			$src['totaldown'] = $this->M_nms->total_nmsdown();

			$header['title'] = "Down | JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "home_down";

			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					// load page
					$this->load->view('header', $header);
					$this->load->view('v_home_down', $src);
				}
			}else{
				$this->load->view('v_login');
			}

		}

	}
