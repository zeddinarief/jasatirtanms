<?php
	class C_customer extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('M_customer');
		}

		function add(){
			// info for header
			$header['title'] = "Add | JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "other";
			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					if($this->session->userdata('posisi') == "admin"){
						$this->load->view('header', $header);
						$this->load->view('v_add');
					}else{
						$this->load->view('header', $header);
						$this->load->view('v_add');
					}
				}
			}else{
				$this->load->view('v_login');
			}
			//$this->load->view('v_add');
		}

		function add_customer(){
			$nama_customer = $this->input->POST('nama_customer');
			$alamat = $this->input->POST('alamat');
			$ip = $this->input->POST('ip');
			$lon = $this->input->POST('lon');
			$lat = $this->input->POST('lat');

			$data = array(
			 'nama_customer' => $nama_customer,
			 'alamat' => $alamat,
			 'ip' => $ip,
			 'latitude' => $lat,
			 'longitude' => $lon
			 );

			$where = array (
				'ip' => $ip
				);

			if($this->M_customer->is_customer_available($where)) {
          echo '<script type="text/javascript">alert("IP sudah terdaftar !!");</script>';
			  	echo "<script>history.go(-1);</script>";
	        }
	        else {
	            $this->M_customer->insert_customer('customer',$data);
							redirect(base_url());
	        }

		}

		function detail($id){
			$where = array (
				'id' => $id
				);

			// info for header
			$header['title'] = "Detail | JasaTirtaNMS";
			$header['posisi'] = $this->session->userdata('posisi');
			$header['page'] = "other";

			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					$this->customer['detailcustomer'] = $this->M_customer->tampildetail('customer',$where);
					// load page
					$this->load->view('header', $header);
					$this->load->view('v_detail', $this->customer);
				}
			}else{
				$this->load->view('v_login');
			}

		}

		function update(){
			$nama_customer = $this->input->POST('nama_customer');
			$id = $this->input->POST('id');
			$alamat = $this->input->POST('alamat');
			$ip = $this->input->POST('ip');
			$lat = $this->input->POST('lat');
			$lon = $this->input->POST('lon');

			$data = array(
			 'nama_customer' => $nama_customer,
			 'alamat' => $alamat,
			 'ip' => $ip,
			 'latitude' => $lat,
			 'longitude' => $lon
			 );

			 $where = array(
				'id' => $id
				);
			$this->M_customer->update($where,$data,'customer');
			redirect(base_url());
		}

		function hapus_customer($id){
			$where = array(
				'id' => $id
				);

			//echo "<script>history.go(-1);</script>";
			if ($this->session->userdata('app') == "nmsjasatirta") {
				if($this->session->userdata('status') != "logged"){
					$this->load->view('v_login');
				}else{
					$this->M_customer->hapus('customer',$where);
					redirect(base_url());
				}
			}else{
				$this->load->view('v_login');
			}
		}

	}
