<?php
class M_nms extends CI_Model{

	function list_pelanggan(){
		$query = $this->db->query("SELECT * FROM customer");
		return $query->result_array();
	}

    function total_nms(){
        $query = $this->db->count_all_results('customer');
        //$query = $this->db->query("SELECT count(*) AS total FROM customer WHERE segment='DBS'");
        return $query;
    }

    function total_nmsdown(){
        $this->db->where('status!=', 'ok');
        $query = $this->db->count_all_results('customer');
        return $query;
    }

    function list_nms($config){
        $hasilquery=$this->db->get('customer', $config['per_page'], $this->uri->segment(3));
        if ($hasilquery->num_rows() > 0) {
            foreach ($hasilquery->result() as $value) {
                $data[]=$value;
            }
            return $data;
        }
    }

    function list_nms_down($config){
        $this->db->where('status!=', 'ok');
        $hasilquery=$this->db->get('customer', $config['per_page'], $this->uri->segment(3));
        if ($hasilquery->num_rows() > 0) {
            foreach ($hasilquery->result() as $value) {
                $data[]=$value;
            }
            return $data;
        }
    }

    function cari($key){
        $query = $this->db->query("SELECT * FROM customer  WHERE nama_customer LIKE '%$key%'");
        return $query->result_array();
    }

	function createcsv(){
    	$tgl_sekarang=date("Y-m-d");
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = "Data Customer ".$tgl_sekarang.".csv";
        $query = "SELECT * FROM customer"; //USE HERE YOUR QUERY
        $result = $this->db->query($query);
        $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        force_download($filename, $data);
    }

}
