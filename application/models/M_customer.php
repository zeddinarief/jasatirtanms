<?php
class M_customer extends CI_Model{

	function insert_customer($table,$data){
        $this->db->insert($table , $data);
	}

	function tampildetail($table,$where){
		$query = $this->db->get_where($table, $where);
		return $query->result_array();
	}

	function update($where,$data,$table){
		$this->db->where($where);
        $this->db->update($table,$data);
	}

	function hapus($table,$where){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function is_customer_available($where) {
    $query = $this->db->get_where("customer",$where);
    if($query->num_rows() > 0) {
        return true;
    }
    else {
        return false;
    }
    }

}
