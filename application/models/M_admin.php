<?php
class M_admin extends CI_Model{	

	function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
	}

    function lihat_pass($table,$where){      
        return $this->db->get_where($table,$where);
    }

    function list_admin($table){        
        $query = $this->db->query("SELECT * FROM admin");
        return $query->result_array();
    }

	function tambah_admin($data,$table){
        $this->db->insert($table , $data);
   	}

    function hapus($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function update_profile($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

   	function is_user_available($where) {  
        
        $query = $this->db->get_where("admin",$where);  
        if($query->num_rows() > 0) {  
            return true;  
        }  
        else {  
            return false;  
        }  
    }

}