  <div id="realtime" class="container" style="margin-bottom: 30px;">

      <div class="row">
        <div id="map"></div>
      </div>
      <div class="row">
        <div style="text-align: center;"><p><b>Total -> <?php echo $total; ?></b></p></div>
        <div style="text-align: center;"><p style="color: red"><b>Total Down -> <?php echo $totaldown; ?></b></p></div>
      </div>
        <?php
          $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $token = explode('jasatirtanms/', $url);
          //echo $token[sizeof($token)-1];
          echo $this->pagination->create_links();

        ?>
 </div>
 <script type="text/javascript">

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }
      // open layers
      var map = new ol.Map({
        target: 'map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM(),
          }),
        ],
        view: new ol.View({
          center: ol.proj.fromLonLat([112.632210, -7.965545]),
          zoom: 10
        })
      });

      // map.on('singleclick', function(evt) {
      //   map.hasFeatureAtPixel(evt.pixel);
      //   if (feature) {
      //     // var feat = map.getFeaturesAtPixel(evt.pixel);
      //
      //     alert(feature.getId());
      //       //here you can add you code to display the coordinates or whatever you want to do
      //   }
      // });

      map.on('click', function(event) {
          map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
            var id = feature.getId();
            if ( id ) {
              // var text = feature.getStyle().getText().getText();
              // alert(text);
              location.href="<?php echo base_url('C_customer/detail/');?>" + id;
              // feature.setStyle(changeStyle(text));

            }
          });
      });
      // var arr = [];
      $.ajax({
          type: 'POST',
          url: "<?php echo base_url('C_nms/nmsdata'); ?>",
          dataType: 'json',
          success: function(msg)
          {
            var arr = [];

              for (var i = 0; i < msg.length; i++)
              {

                var id = parseInt(msg[i].id);
                var lon = parseFloat(msg[i].longitude);
                var lat = parseFloat(msg[i].latitude);
                var stat = msg[i].status;
                // console.log('lon ' +typeof(lon) + lon);
                // arr.push(addMarker(lon, lat, id, stat));
                addMarker(lon, lat, id, stat);
                // console.log(id + lon + lat + id + stat);
                // console.log(msg[i]);
              }

              // buatMarker(arr);
          }
      });

      function addMarker(lon, lat, id, stat){
        var marker, styleOk, styleNotok, sourceVector, layerVector;
        marker = new ol.Feature({
          geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat])),
          name: 'marker'
        });

        if(stat == 'ok'){
          styleMarker = new ol.style.Style({
            image: new ol.style.Icon({
              scale: .01,
              src: '<?php echo base_url("assets/image/konek.jpg"); ?>',
              anchor: [.5, .5],
            }),
          });
        }else {
          styleMarker = new ol.style.Style({
            image: new ol.style.Icon({
              scale: .01,
              src: '<?php echo base_url("assets/image/gakonek.jpg"); ?>',
              anchor: [.5, .5],
            }),
          });
        }

        sourceVector = new ol.source.Vector();
        layerVector = new ol.layer.Vector({
          source: sourceVector,
        });
        marker.setId(id);
        marker.setStyle(styleMarker);
        if (stat == 'ok') {
            marker.setStyle(styleOk);
        } else {
            marker.setStyle(styleNotok);
        }

        sourceVector.addFeature(marker);
        this.map.addLayer(layerVector);
        console.log('membuat');
        // return marker;
      }
      function buatMarker(marker){
        var sourceVector = new ol.source.Vector();
        var layerVector = new ol.layer.Vector({
          source: sourceVector,
          renderBuffer: 2000
        });
        sourceVector.addFeatures(marker);
        this.map.addLayer(layerVector);
        console.log('jadi');
      }
      function changeStyle(stat){
        if(stat == 'ok'){
          var styleMarker = new ol.style.Style({
            image: new ol.style.Icon({
              scale: .01,
              src: '<?php echo base_url("assets/image/konek.jpg"); ?>',
              anchor: [.5, .5],
            }),
          });
          return styleMarker;
        }else if (stat == 'not_ok') {

          var styleMarker = new ol.style.Style({
            image: new ol.style.Icon({
              scale: .01,
              src: '<?php echo base_url("assets/image/gakonek.jpg"); ?>',
              anchor: [.5, .5],
            }),
          });
          return styleMarker;
        }

      }

      setInterval(function(){
         // $('#realtime').load("<?php echo base_url($token[sizeof($token)-1]);?>" + " #realtime");
         // realtime peta
         $.ajax({
             type: 'POST',
             url: "<?php echo base_url('C_nms/nmsdata'); ?>",
             dataType: 'json',
             success: function(msg)
             {
               var arr = [];

                 for (var i = 0; i < msg.length; i++)
                 {

                   var id = parseInt(msg[i].id);
                   var lon = parseFloat(msg[i].longitude);
                   var lat = parseFloat(msg[i].latitude);
                   var stat = msg[i].status;
                   // console.log('lon ' +typeof(lon) + lon);
                   // arr.push(addMarker(lon, lat, id, stat));
                   // addMarker(lon, lat, id, stat);
                   map.forEachFeatureAtPixel(map.getPixelFromCoordinate(ol.proj.fromLonLat([lon, lat])), function(feature,layer) {
                     var id = feature.getId();
                     if ( id ) {
                       // var text = feature.getStyle().getText().getText();
                       // alert(text);

                       feature.setStyle(changeStyle(stat));

                     }
                   });
                   // console.log(id + lon + lat + id + stat);
                   // console.log(msg[i]);
                   // sleep(500);
                 }

                 // buatMarker(arr);
             }
         });
      }, 5000) /* time in milliseconds */

  </script>
</body>
</html>
