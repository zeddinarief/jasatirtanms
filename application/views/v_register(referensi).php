<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>"
	 rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrap-theme.min.css')?>" rel="stylesheet">
  	<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">  
  	<link href="<?php echo base_url('assets/css/login.css')?>" rel="stylesheet">  
	<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js');?>">  </script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</head>
<body>
	<style align="center" type="text/css">
        .container {
        	margin: 180px auto;
        width: 250px auto;
    </style>

<nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <p class="navbar-text" style="font-weight: bold; font-size: 14; margin-left: 100px;"><marquee> Pandhawa Network Monitoring System </marquee></p>
      </div>
      <ul style="margin-right: 100px" class="nav navbar-nav navbar-right">
        <li>
          <input style="margin-top: 9px" type="button" class="btn btn-danger" value="Koneksi Down" onclick="location.href='<?php echo base_url();?>c_nms_down/down'" />
        </li>
        <li>
          <input style="margin-top: 9px" type="button" class="btn btn-primary" value="Semua Koneksi" onclick="location.href='<?php echo base_url();?>'" />
        </li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $this->session->userdata("nama");?>
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url();?>c_customer/add">Add Customer</a></li>
            <li><a href="<?php echo base_url();?>c_nms/download">Download All Data</a></li>
            <li><a href="<?php echo base_url();?>c_admin/profile">Profil Saya</a></li>
            <li><a href="<?php echo base_url();?>c_login/logout">Logout</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-right" action="<?php echo base_url();?>c_nms_search/search" method="GET">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Cari Company.." name="search">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
  </nav>
<div class="container">
    <div class="row">
		<div class="col-md-4 col-md-offset-4">
    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title">Register</h3>
			 	</div>
			  	<div class="panel-body">
			    	<form action="<?php echo base_url('c_login/aksi_regis'); ?>" method="POST" accept-charset="UTF-8" role="form">
                    <fieldset>
                          	<div class="form-group">
      			    		    <input class="form-control" placeholder="Nama" name="nama" type="text" required="">
      			    		</div>
      			    	  	<div class="form-group">
      			    		    <input class="form-control" id="inputnik" placeholder="username(NIK)" name="nik" type="text" required="">
      			    		    <span id="user_check" class="form-control label"></span>
      			    		</div>
      			    		<div class="form-group">
      			    			<input class="form-control" placeholder="Password" name="password" type="password" required="">
      			    		</div>
      			    		<div class="form-group">
      			    			<input class="form-control" placeholder="Confirm Password" name="confirmpass" type="password" required="">
      			    		</div>
      			    		<div style="text-align: center;" class="form-group">
      			    			<label class="control-label">
      			    				<a href="<?php echo base_url(); ?>">Login</a>
      			    			</label>
      			    		</div>
      			    		<input class="btn btn-lg btn-success btn-block" type="submit" value="Sign In">
      			    	</fieldset>
			      	</form>
			    </div>
			</div>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">
 $(document).ready(function(){  
        $('#inputnik').change(function(){  
            var username = $('#inputnik').val();  
            if(username != '') {  
                $.ajax({  
                    url:"<?php echo base_url('c_login/cek_ketersediaan_user'); ?>",  
                    method:"POST",  
                    data:{username},  
                    success:function(data){ 
                        $('#user_check').html(data);  
                    }  
                });  
            }else{
            	$('#user_check').html('');
            }
            
        });  
    });  
</script>
</html>