	<div class="container">
		<div class="row">
	      <!-- left column -->
	        <h3 style="text-align: center; margin-top: 30px">Data Customer</h3>
	      <!-- edit form column -->
	      <div class="col-md-12 personal-info">
	        <?php foreach ($detailcustomer as $i) {?>
	        <form action="<?php echo base_url('C_customer/update');?>" method="POST" class="form-horizontal" role="form">
	          <hr>

	      	  <div class="form-group">
	      	  	<div class="col-lg-3 control-label">
	            	<a href="<?php echo base_url(); ?>" class="btn btn-default" >back</a>
	      	  	</div>
	            <div class="col-lg-8">
	              <input class="form-control" name="id" value="<?php echo $i['id'];?>" type="hidden" required="">
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-lg-3 control-label">Nama Customer</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="nama_customer" value="<?php echo $i['nama_customer'];?>" type="text" required="">
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-md-3 control-label">Alamat</label>
	            <div class="col-md-8">
	              <input class="form-control" name="alamat" value="<?php echo $i['alamat'];?>" type="text" required="">
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-lg-3 control-label">IP Address</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="ip" value="<?php echo $i['ip'];?>" type="text" required="">
	            </div>
	          </div>

						<div class="form-group">
	            <label class="col-lg-3 control-label">Koordinat</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="lat" placeholder="Latitude" value="<?php echo $i['latitude'];?>" type="text" required="">
								<input class="form-control" name="lon" placeholder="Longitude" value="<?php echo $i['longitude'];?>" type="text" required="">
	            </div>
	          </div>

	          <?php } ?>

	          <div class="form-group">
	            <label class="col-md-3 control-label"></label>
	            <div class="col-md-8">
	              <input class="btn btn-primary" value="Save Changes" onclick='return confirm("Anda yakin ingin mengupdate Pelanggan `"+"<?php echo $i['nama_customer'];?> "+"` ?")' type="submit">
	              <span></span>
	              <input class="btn btn-default" value="Cancel" type="reset">
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="col-md-3 control-label"></label>
	            <div class="col-md-8">
	              <a href="<?php echo base_url('C_customer/hapus_customer/');echo $i['id'];?>" class="btn btn-danger" onclick='return confirm("Anda yakin ingin menghapus Pelanggan `"+"<?php echo $i['nama_customer'];?> "+"` ?")'>Hapus Pelanggan ini</a>
	            </div>
	          </div>
	        </form>
	      </div>
	     </div>
	   	</div>
</body>
</html>
