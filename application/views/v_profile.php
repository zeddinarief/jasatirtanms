<div class="container">
		<div class="row">
	      <!-- left column -->
	        <h3 style="text-align: center; margin-top: 30px">Data Profil</h3>
	      <!-- edit form column -->
	      <div class="col-md-12 personal-info">

	        <form action="<?php echo base_url('C_admin/update_profile');?>" method="POST" class="form-horizontal" role="form">
	          <hr>

	      	  <div class="form-group">
	      	  	<div class="col-lg-3 control-label">
	            	<a href="<?php echo base_url(); ?>" class="btn btn-default" >back</a>
	      	  	</div>
	          </div>

	          <div class="form-group">
	            <label class="col-lg-3 control-label">NIK</label>
	            <div class="col-lg-8">
	              <label class="control-label"><?php echo $this->session->userdata('NIK');?></label>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-lg-3 control-label">Nama</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="nama" value="<?php echo $this->session->userdata('nama');?>" type="text" required="">
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-md-3 control-label"></label>
	            <div class="col-md-8">
	              <input class="btn btn-primary" value="Save Changes" onclick='return confirm("Anda yakin ingin mengupdate profil anda ?")' type="submit">
	              <span></span>
	              <input class="btn btn-default" value="Cancel" type="reset">
	            </div>
	          </div>
	        </form>

	        <form action="<?php echo base_url()?>C_admin/update_pass" style="margin-top: 40px" method="POST" class="form-horizontal" role="form">
	        	<p style="font-size: 16px; text-align: center;"><b>Ganti Password</b></p>
	          <div class="form-group">
	            <label class="col-md-3 control-label">Old Password:</label>
	            <div class="col-md-8">
	              <input class="form-control" name="oldpassword" type="password">
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="col-md-3 control-label">New Password:</label>
	            <div class="col-md-8">
	              <input class="form-control" name="newpassword" type="password">
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="col-md-3 control-label">Confirm New Password:</label>
	            <div class="col-md-8">
	              <input class="form-control" name="confirmpassword" type="password">
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="col-md-3 control-label"></label>
	            <div class="col-md-8">
	              <input class="btn btn-primary" value="Save Changes" onclick='return confirm("Anda yakin ingin mengupdate password anda ?")' type="submit">
	              <span></span>
	              <input class="btn btn-default" value="Cancel" type="reset">
	            </div>
	          </div>
	        </form>

	      </div>
	     </div>
	   	</div>
</body>
</html>
