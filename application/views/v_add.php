  <div class="container">
    <div class="row">
        <!-- left column -->
          <h3 style="text-align: center; margin-top: 30px">Tambah Customer</h3>
        <!-- edit form column -->
        <div class="col-md-12 personal-info">
          <form action="<?php echo base_url('C_customer/add_customer');?>" method="POST" class="form-horizontal" role="form">
            <hr>

            <div class="form-group">
              <div class="col-lg-3 control-label">
                <a href="<?php echo base_url(); ?>" class="btn btn-default" >back</a>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Nama Customer</label>
              <div class="col-lg-8">
                <input class="form-control" name="nama_customer" placeholder="Nama Customer" type="text" required="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Alamat</label>
              <div class="col-md-8">
                <input class="form-control" name="alamat" placeholder="Alamat" type="text" required="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">IP Address</label>
              <div class="col-lg-8">
                <input class="form-control" name="ip" placeholder="IP Address" type="text" required="">
              </div>
            </div>

            <div class="form-group">
	            <label class="col-lg-3 control-label">Koordinat</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="lat" placeholder="Latitude" type="text" required="">
								<input class="form-control" name="lon" placeholder="Longitude" type="text" required="">
	            </div>
	          </div>

            <div class="form-group">
              <label class="col-md-3 control-label"></label>
              <div class="col-md-8">
                <input class="btn btn-primary" value="Add" type="submit">
                <span></span>
                <input class="btn btn-default" value="Reset" type="reset">
              </div>
            </div>
          </form>
        </div>
       </div>
      </div>
</body>
</html>
