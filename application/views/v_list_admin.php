<div class="container" style="margin-bottom: 30px">
  <div style="margin-bottom: 20px; margin-left: 10px" class="row">
    <a data-toggle="modal" data-target="#registration" class="btn btn-default" type="button" style="border: 1px #CCC solid;">Tambah User/Admin</a>
        </li>
  </div>
    <div class="table-responsive">
     <table class="table table-bordered table-striped">
      <thead>
      <tr>
       <th style="width: 10px; text-align: center;" class="col-md-1">No</th>
       <th style="text-align: center;" class="col-md-5">Nama</th>
       <th style="text-align: center;" class="col-md-2">Role</th>
       <th style="text-align: center;" class="col-md-2"></th>
       <th style="text-align: center;" class="col-md-2"></th>
      </tr>
      </thead>
      <?php
           $no = 1;
           foreach ($admin as $data){
       ?>
      <tbody>
      <tr>
       <td style="width: 10px; text-align: center;" class="col-md-1"><?php echo $no++ ?></td>
       <td class="col-md-5"><?php echo $data['nama']; ?></td>
       <td style="text-align: center;" class="col-md-2"><?php echo $data['posisi']; ?></td>
       <td style="text-align: center;" class="col-md-2"><a onclick='lihatPass("<?php echo $data['NIK']; ?>")' id="lihatpssword" data-toggle="modal" data-target="#lihatPass">lihat password</a></td>
       <td style="text-align: center;" class="col-md-2"><a href="<?php echo base_url('C_admin/hapus_admin/');echo $data['NIK'];?>" class="btn btn-danger" onclick='return confirm("Anda yakin ingin menghapus Admin dengan nama `"+"<?php echo $data['nama'];?> "+"` ?")'>Hapus</a></td>
      </tr>
      </tbody>
       <?php
        }
       ?>

     </table>

     <div style="background-color: white; text-align: center;"><p><b>Total User/Admin <?php echo $no -= 1; ?></b></p></div>
    </div>
    <div id="registration" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- konten modal-->
              <div class="modal-content">
                <!-- body modal -->
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="container">
                    <div class="row">
                        <div class="col-md-6" style="margin-left: -7px;">
                        <h3 style="text-align: center;">Register</h3>
                        <form action="<?php echo base_url('C_login/aksi_regis'); ?>" method="POST" accept-charset="UTF-8" role="form">
                              <fieldset>
                                <div class="form-group">
                            <input class="form-control" placeholder="Nama" name="nama" type="text" required="">
                        </div>
                          <div class="form-group">
                            <input class="form-control" id="inputnik" placeholder="username(NIK)" name="nik" type="text" required="">
                            <span id="user_check" class="form-control label"></span>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Role</label>
                          <div>
                            <select class="form-control" name="posisi" required="">
                              <option></option>
                              <option>user</option>
                              <option>admin</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <input class="form-control" placeholder="Password" name="password" type="password" required="">
                        </div>
                        <div class="form-group">
                          <input class="form-control" placeholder="Confirm Password" name="confirmpass" type="password" required="">
                        </div>
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Register">
                      </fieldset>
                        </form>
                      </div>
                    </div>
                    <!-- Modal -->
                  </div>
                </div>
              </div>
            </div>
          </div>
      <div id="lihatPass" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- konten modal-->
              <div class="modal-content">
                <!-- body modal -->
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="container">
                    <div class="row">
                        <div class="col-md-6" style="margin-left: -7px;">
                        <form action="<?php echo base_url('C_admin/lihatPass'); ?>" method="POST" accept-charset="UTF-8" role="form">
                              <fieldset>
                                <div class="form-group">
                                    <input class="form-control" id="niktujuan" name="niktujuan" type="hidden" value="" >
                                    <input class="form-control" placeholder="Masukkan NIK tujuan" name="nik" type="text" required="">
                                </div>
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Lihat Password">
                              </fieldset>
                        </form>
                      </div>
                    </div>
                    <!-- Modal -->
                  </div>
                </div>
              </div>
            </div>
          </div>
 </div>

</body>
<script type="text/javascript">
 $(document).ready(function(){
        $('#inputnik').change(function(){
            var username = $('#inputnik').val();
            if(username != '') {
                $.ajax({
                    url:"<?php echo base_url('C_login/cek_ketersediaan_user'); ?>",
                    method:"POST",
                    data:{username},
                    success:function(data){
                        $('#user_check').html(data);
                    }
                });
            }else{
              $('#user_check').html('');
            }

        });
    });
 function lihatPass(nik){
    var elem = document.getElementById("niktujuan");
    elem.value = nik;
 }
</script>
</html>
