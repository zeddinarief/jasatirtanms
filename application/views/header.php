<!DOCTYPE html>
<html>
<head>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/home.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- opsional browser lawas -->
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList"></script>
    <!-- include peta openlayers < wajib -->
    <script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>

  <title><?php echo $title;?></title>
  <style>
    #map {
      height: 500px;
      width: 100%;
    }
  </style>

</head>
<body>
  <!-- header user -->
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <p class="navbar-text" style="font-weight: bold; font-size: 14; margin-left: 100px;"><marquee> Jasa Tirta Network Monitoring System </marquee></p>
      </div>
      <ul style="margin-right: 100px" class="nav navbar-nav navbar-right">
        <?php if ($page == "home_down"): ?>
          <li>
            <input style="margin-top: 9px" type="button" class="btn btn-primary" value="Semua Koneksi" onclick="location.href='<?php echo base_url();?>'" />
          </li>
        <?php elseif ($page == "home"): ?>
          <li>
            <input style="margin-top: 9px" type="button" class="btn btn-danger" value="Koneksi Down" onclick="location.href='<?php echo base_url('C_nms_down/down');?>'" />
          </li>
        <?php else: ?>
          <li>
            <input style="margin-top: 9px" type="button" class="btn btn-danger" value="Koneksi Down" onclick="location.href='<?php echo base_url();?>C_nms_down/down'" />
          </li>
          <li>
            <input style="margin-top: 9px" type="button" class="btn btn-primary" value="Semua Koneksi" onclick="location.href='<?php echo base_url();?>'" />
          </li>
        <?php endif; ?>

        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $this->session->userdata("nama");?>
          <span class="caret"></span></a>
          <?php if ($posisi == "admin"): ?>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url();?>C_customer/add">Add Customer</a></li>
              <li><a href="<?php echo base_url();?>C_nms/download">Download All Data</a></li>
              <li><a href="<?php echo base_url();?>C_admin/daftar_admin">Daftar Admin/User</a></li>
              <li><a href="<?php echo base_url();?>C_admin/profile">Profil Saya</a></li>
              <li><a href="<?php echo base_url();?>C_login/logout">Logout</a></li>
            </ul>
          <?php else: ?>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url();?>C_customer/add">Add Customer</a></li>
              <li><a href="<?php echo base_url();?>C_nms/download">Download All Data</a></li>
              <li><a href="<?php echo base_url();?>C_admin/profile">Profil Saya</a></li>
              <li><a href="<?php echo base_url();?>C_login/logout">Logout</a></li>
            </ul>
          <?php endif; ?>

        </li>
      </ul>
      <form class="navbar-form navbar-right" action="<?php echo base_url();?>C_nms_search/search" method="GET">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Cari Company.." name="search">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit" >
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
  </nav>
  <!-- header user-->
