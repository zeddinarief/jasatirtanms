-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 10, 2019 at 04:05 PM
-- Server version: 10.3.13-MariaDB-1
-- PHP Version: 7.2.9-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jasatirtanms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `NIK` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `posisi` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`NIK`, `nama`, `password`, `posisi`) VALUES
('1234', 'ZEDDIN ARIEF', 'admin', 'admin'),
('18890071', 'Yugo Deksino', 'yugodeksino', 'admin'),
('38091', 'yuda', 'telkom', 'admin'),
('38852', 'Dimas Setyawan', 'Dimas', 'admin'),
('47932', 'Yunus Pradana', 'bismillah31', 'admin'),
('user', 'user', 'user', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama_customer` varchar(100) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `alamat` varchar(200) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama_customer`, `ip`, `status`, `alamat`, `latitude`, `longitude`) VALUES
(4, 'google', '8.8.8.8', 'ok', 'world', -7.916901, 112.634247),
(5, 'pc gembong', '192.168.100.100', 'not_ok', 'omah', -7.918476, 112.644956),
(6, 'pc coba', '192.168.1.5', 'not_ok', 'coba', -7.965545, 112.63221),
(8, 'pc daan', '192.168.100.32', 'ok', 'omah daan', -7.961089, 112.61835);

-- --------------------------------------------------------

--
-- Table structure for table `down`
--

CREATE TABLE `down` (
  `nama` varchar(200) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `time` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `down`
--

INSERT INTO `down` (`nama`, `ip`, `time`) VALUES
('pc coba', '192.168.1.4', 1554205660),
('pc coba', '192.168.1.5', 1554818565),
('pc gembong', '192.168.100.100', 1554818063);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`NIK`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ip` (`ip`);

--
-- Indexes for table `down`
--
ALTER TABLE `down`
  ADD PRIMARY KEY (`ip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
